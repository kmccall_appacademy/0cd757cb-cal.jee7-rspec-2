def reverser(&prc)
  string = prc.call
  string.split.map(&:reverse).join(" ")
end

def adder(start = 1, &prc)
  start + prc.call
end

def repeater(num = 1)
  (0...num).each { yield }
end
