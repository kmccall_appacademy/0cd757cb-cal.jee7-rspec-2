require "time"

def measure(n = 1, &prc)
  start_time = Time.now
  n.times{prc.call}
  time = Time.now - start_time
  average_time = time / n
  average_time
end
